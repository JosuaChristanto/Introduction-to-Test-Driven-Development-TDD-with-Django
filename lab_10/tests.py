from django.test import TestCase
from django.test import Client
import environ
from .views import *
from .utils import *
from .csui_helper import *
from .custom_auth import *
from .models import *


env = environ.Env(DEBUG=(bool, False),)

class Lab10UnitTest(TestCase):
    def test_lab_10_session_url_is_exist(self):
        response = Client().get('/lab-10/')
        self.assertEqual(response.status_code, 200)

    def test_lab_10_session_login_page_is_working(self):
        response = Client().get('/lab-10/')
        html_response = response.content.decode('utf8')
        self.assertIn("Gunakan <b> akun SSO </b> untuk login",html_response)

    def test_dashboard(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")
        response_post = self.client.post('/lab-10/custom_auth/login/', {'username': self.username, 'password': self.password})
        response = self.client.get('/lab-10/dashboard/',follow=True)
        html_response = response.content.decode('utf8')
        self.assertIn('hafiyyan.sayyid',html_response)

    def test_add_watch_later_is_working(self):
        response = Client().get('/lab-10/movie/detail/tt2096673/')
        html_response = response.content.decode('utf8')
        self.assertIn('Inside Out',html_response)
        self.assertIn('Add to Watch Later',html_response)
        response = Client().get('/lab-10/movie/watch_later/add/tt2096673/',follow=True)
        html_response = response.content.decode('utf8')
        self.assertIn('Added to Watch Later',html_response)
        self.client.post(reverse('lab-10:api_search_movie', kwargs = {'judul':'hahaha', 'tahun':'2010'}))
        self.client.post(reverse('lab-10:movie_list'), data = {'judul':'hahaha', 'tahun':'2010'})
        response = self.client.get('lab-10:dashboard')
        self.client.post(reverse('lab-10:add_watch_later',kwargs={'id':'tt2096673'}))
        response = self.client.get(reverse('lab-10:movie_detail',kwargs={'id':'tt2096673'}))
        self.assertEqual(response.content.decode('utf8'),'2010')
        

    def test_csui_helper(self):
        self.assertEqual(get_client_id(),'X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG')
        verify_user('X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG')
        get_access_token('hafiyyan.sayyid','password')
        get_data_user('w','w')
        self.assertTrue(True)

    def test_omdb(self):
        result = search_movie('dunkirk','-')
        self.assertEqual(len(result),10)
        result = get_detail_movie('tt5013056')
        self.assertEqual(result['title'].decode('utf8'),'Dunkirk')

    def test_lab10_custom_auth(self):
        response = self.client.post(reverse('lab-10:auth_login'),
                                    data={
                                        'username': 'hehe',
                                        'password': 'hehe'
                                    }, follow=True)
        self.assertIn('salah', response.content.decode('utf-8'))

        response = self.client.get(reverse('lab-10:index'))
        self.assertIn('Login', response.content.decode('utf-8'))
        response = self.client.get(reverse('lab-10:dashboard'), follow=True)
        self.assertIn('Login', response.content.decode('utf-8'))

        self.client.post(reverse('lab-10:auth_login'), data={
            'username': env("SSO_USERNAME"),
            'password': env("SSO_PASSWORD"),
        }, follow=True)

        response = self.client.get(reverse('lab-10:index'), follow=True)
        self.assertNotIn('Login', response.content.decode('utf8'))
        reponse = self.client.get(reverse('lab-10:auth_logout'), follow=True)
        # self.assertIn('Anda berhasil logout. Semua session Anda sudah dihapus', response.content.decode('utf8'))

    def test_auth_logout(self):
        response_post = self.client.post('/lab-10/custom_auth/logout/')
        response = self.client.get('/lab-10/',follow=True)
        html_response = response.content.decode('utf8')
        self.assertIn('Anda berhasil logout. Semua session Anda sudah dihapus',html_response)
        response = self.client.post(reverse('lab-10:auth_logout'),follow=True)
        self.assertIn('Anda berhasil logout. Semua session Anda sudah dihapus',html_response)

 // FB initiation function
  window.fbAsyncInit = () => {
    FB.init({
      appId      : '536091000061770',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.11'
    });

    FB.getLoginStatus(function(response){
      if(response.status === 'connected'){
            // the user is logged in and has authenticated your
    // app, and response.authResponse supplies
    // the user's ID, a valid access token, a signed
    // request, and the time the access token 
    // and signed request each expire
    var uid = response.authResponse.userID;
    var accessToken = response.authResponse.accessToken;
    console.log('sudah berhasil login')
      render(true);
      }
      else if(response.status === 'not_authorized'){
        console.log('not authorized')
        render(false);
      }
      else{
        console.log("not login");
        render(false);
      }
    });
    // implementasilah sebuah fungsi yang melakukan cek status login (getLoginStatus)
    // dan jalankanlah fungsi render di bawah, dengan parameter true jika
    // status login terkoneksi (connected)

    // Hal ini dilakukan agar ketika web dibuka dan ternyata sudah login, maka secara
    // otomatis akan ditampilkan view sudah login
  };

  // Call init facebook. default dari facebook
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

  // Fungsi Render, menerima parameter loginFlag yang menentukan apakah harus
  // merender atau membuat tampilan html untuk yang sudah login atau belum
  // Ubah metode ini seperlunya jika kalian perlu mengganti tampilan dengan memberi
  // Class-Class Bootstrap atau CSS yang anda implementasi sendiri
  const render = loginFlag => {
    if (loginFlag) {
      // Jika yang akan dirender adalah tampilan sudah login

      // Memanggil method getUserData (lihat ke bawah) yang Anda implementasi dengan fungsi callback
      // yang menerima object user sebagai parameter.
      // Object user ini merupakan object hasil response dari pemanggilan API Facebook.
       getUserData(user => {
        console.log('sudah masuk callbacknya');
        // Render tampilan profil, form input post, tombol post status, dan tombol logout
        $('#lab8').html(
          '<div class="container databot">'+ '<div class="col-md-8">'+
          '<div class="profile">' +
          '<div class="tempat-foto">'+
            '<img class="cover" src="' + user.cover.source + '" alt="cover" />' +
            '<img class="picture" src="' + user.picture.data.url + '" alt="profpic" /></div>' +
            '<div class="data">' +
              '<h1>' + user.name + '</h1>' +
              '<h2>' + user.about + '</h2>' +
              '<h3>' + user.email + ' - ' + user.gender + '</h3>' +
            '</div>' +
          '</div>' +
          '</div>'+
          
          '<div class="col-md-4">'+'<div class="form-group">'+
          '<label for="comment">Comment:</label><textarea class="form-control" rows="5" id="postInput"  placeholder="Ketik Status Anda"></textarea></div>'+
          '<button class="postStatus btn btn-success" onclick="postStatus()">Post ke Facebook</button> ' +
          '<button class="logout btn btn-danger" onclick="facebookLogout()">Logout</button>'+'</div>'+'</div>'
        );

        // Setelah merender tampilan di atas, dapatkan data home feed dari akun yang login
        // dengan memanggil method getUserFeed yang kalian implementasi sendiri.
        // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
        // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
        getUserFeed(feed => {
          $('#lab8').append('<div class="d-flex flex-column">');
          feed.data.map(value => {
            // Render feed, kustomisasi sesuai kebutuhan.
            if (value.message && value.story) {

              $('#lab8').append(
                '<div class="feed part1 p-2">' +
                  '<h1>' + value.message + '</h1>' +
                  '<p>' + value.story + '</p>' +
                  '<button class="btn btn-danger">delete</div>'+
                '</div>'
              );
            } else if (value.message) {


              $('#lab8').append(
                '<div class="feed part2 p-2">' +
                  '<p>' + value.message + '</p>' +
                  '<button class="btn btn-danger">delete</div>'+
                '</div>'
              );
            } else if (value.story) {
              $('#lab8').append(
                '<div class="feed part3 p-2">' +
                  '<p>' + value.story + '</p>' +
                  '<button class="btn btn-danger">X</div>'+
                '</div>'
              );
            }
          });
          $('#lab8').append('</div>');
        });
      });
    } else {
      // Tampilan ketika belum login
      var html_back = '<div class="container">'+
    '<div class="col-md-4"></div>'+
    '<div class="col-md-4 center-block">'+
          '<a class="btn btn-block btn-social btn-facebook btn-lg" onclick="facebookLogin()">'+
    '<span class="fa fa-facebook"></span> Sign in with Facebook'+'</a>'+'</div>'+'<div class="col-md-4"></div>'+'</div>'+'</div>';
      $('#lab8').html(html_back);
    }
  };

  const facebookLogin = () => {
        // TODO: Implement Method Ini
    // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan sudah login
    // ketika login sukses, serta juga fungsi ini memiliki segala permission yang dibutuhkan
    // pada scope yang ada. Anda dapat memodifikasi fungsi facebookLogin di atas.
    FB.login(function(response) { //method for asking user authorization
      if (response.authResponse) {
        console.log('Welcome!  Fetching your information.... ');
        FB.api('/me', function(response) {
          console.log('Good to see you, ' + response.name + '.');
        });
      } else {
       console.log('User cancelled login or did not fully authorize.');
      }
    }, {scope:'public_profile,user_posts,publish_actions'});
    window.location.reload(); // melakukan reload secara otomatis
  };


  const facebookLogout = () => {
        FB.getLoginStatus(function(response) {
      if (response.status === 'connected') {
        FB.logout();
      }
    });
    window.location.reload();

    // TODO: Implement Method Ini
    // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan belum login
    // ketika logout sukses. Anda dapat memodifikasi fungsi facebookLogout di atas.
  };

  // TODO: Lengkapi Method Ini
  // Method ini memodifikasi method getUserData di atas yang menerima fungsi callback bernama fun
  // lalu merequest data user dari akun yang sedang login dengan semua fields yang dibutuhkan di 
  // method render, dan memanggil fungsi callback tersebut setelah selesai melakukan request dan 
  // meneruskan response yang didapat ke fungsi callback tersebut
  // Apakah yang dimaksud dengan fungsi callback?


  const getUserData = (fun) => {
    FB.api("/me?fields=name,about,email,gender,cover,picture", 'GET', function (response){
      if(response && !response.error){
      console.log('ready to catch data')
      fun(response);
    }
    });
  };
  //https://developers.facebook.com/docs/javascript/reference/FB.api/
  const getUserFeed = (fun) => {
    FB.api("/me/feed", function(response){
      if(response && !response.error){
      console.log('get user feed')
      fun(response);
    }
    });
    // TODO: Implement Method Ini
    // Pastikan method ini menerima parameter berupa fungsi callback, lalu merequest data Home Feed dari akun
    // yang sedang login dengan semua fields yang dibutuhkan di method render, dan memanggil fungsi callback
    // tersebut setelah selesai melakukan request dan meneruskan response yang didapat ke fungsi callback
    // tersebut
  };

  const postFeed = (message) => {
    console.log(message);
    FB.api('/me/feed', 'POST', {message:message});
        window.location.reload();
  };


  const postStatus = () => {
    const message = $('#postInput').val();
    console.log("ready to post status")
    postFeed(message);

  };

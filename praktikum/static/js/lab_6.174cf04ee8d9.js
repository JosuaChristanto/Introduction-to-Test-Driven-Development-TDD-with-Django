
$('.chat-head').click(function(){
	$('.chat-body').toggle();
    
});
// kode jQuery selanjutnya akan ditulis disini
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
    /* implementasi clear all */
    print.value = "";
      } else if (x === 'eval') {       
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  }
  else if(x === 'sin'){
    print.value = Math.round(evil(Math.sin(print.value)) * 10000) / 10000;
    // print.value = Math.sin(print.value);
    erase = true;
  }
    else if(x === 'log'){
      print.value = Math.log(print.value);
       erase = true;
    }
      else if(x === 'tan'){
        print.value = Math.tan(print.value);
        erase = true;
      }
   else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}

var themesOri = [
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
];
var defaultTheme = {"Indigo":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}};

if (localStorage.getItem('themes') == null){ localStorage.setItem('themes',JSON.stringify(themesOri)); }
if (localStorage.getItem('selectedTheme') == null) { localStorage.setItem('selectedTheme', JSON.stringify(defaultTheme)); }
var getThemes = JSON.parse(localStorage.getItem('themes'));
var getSelectedTheme = JSON.parse(localStorage.getItem('selectedTheme'));
changeTheme(getThemes[3]);

$(document).ready(function() {
    $('.my-select').select2({'data' : getThemes}).val(getSelectedTheme['id']);
    $('.apply-button').on('click', function(){
        getSelectedTheme = getThemes[$('.my-select').val()];
        changeTheme(getSelectedTheme);
        localStorage.setItem('selectedTheme',JSON.stringify(getSelectedTheme));
    })
});

function changeTheme(x){
    $('body').css({"backgroundColor": x.bcgColor});
    $('.text-center').css({"color": x.fontColor});
}

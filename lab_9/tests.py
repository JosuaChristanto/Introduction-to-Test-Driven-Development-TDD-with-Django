from django.test import TestCase
from django.test import Client
from django.urls import resolve, reverse
from .views import index, set_data_for_session, profile, add_session_drones, \
				del_session_drones, clear_session_drones, cookie_login, \
				cookie_auth_login, cookie_profile, cookie_clear, \
				my_cookie_auth, is_login, add_session_item, del_session_item, \
				clear_session_item
from .views import *
from .api_enterkomputer import *
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse, HttpResponseNotFound, HttpRequest
from .custom_auth import auth_login, auth_logout
from .csui_helper import get_data_user, verify_user, get_client_id, get_access_token
import environ


env = environ.Env(DEBUG=(bool, False),)


class Lab9UnitTest(TestCase):
    def test_lab_9_session_url_is_exist(self):
        response = Client().get('/lab-9/')
        self.assertEqual(response.status_code, 200)

    def test_lab9_session_using_index_func(self):
        found = resolve('/lab-9/')
        self.assertEqual(found.func, index)

    def test_lab_9_session_login_page_is_working(self):
        response = Client().get('/lab-9/')
        html_response = response.content.decode('utf8')
        self.assertIn("Gunakan <b> akun SSO </b> untuk login",html_response)


########### test cookie di view dlu yuk #############
    def test_lab_9_cookie_url_is_exist(self):
        response = Client().get('/lab-9/cookie/login/')
        self.assertEqual(response.status_code, 200)

    def test_lab9_cookie_using_func(self):
        found = resolve('/lab-9/cookie/login/') #melakukan tes pada cookie/login/
        self.assertEqual(found.func, cookie_login) #apakah alamat tersebut dirender oleh cookie_login

    def test_lab9_my_cookie_auth(self):
    	self.assertEqual(my_cookie_auth('utest','ptest'), True)
    	self.assertEqual(my_cookie_auth('mama','mia'), False)

    def test_lab_9_cookie_login_page_is_working(self):
        response = Client().get('/lab-9/cookie/login/')
        html_response = response.content.decode('utf8')
        self.assertIn("Jangan menggunakan <b> akun SSO asli </b>",html_response)

    def test_lab_9_cookie_bad_login_is_working(self):
        response = Client().post('/lab-9/cookie/auth_login/',{'username':'a','password':'a'}, follow=True)
        html_response = response.content.decode('utf8')
        self.assertIn("Jangan menggunakan <b> akun SSO asli </b>",html_response)
        response = Client().get('/lab-9/cookie/auth_login/',{'username':'a','password':'a'}, follow=True)
        html_response = response.content.decode('utf8')
        self.assertIn("Jangan menggunakan <b> akun SSO asli </b>",html_response)
        response = self.client.get(reverse('lab-9:cookie_profile'), follow=True)
        self.assertIn('Login', response.content.decode('utf8'))


    def test_lab_9_cookie_good_login_is_working(self):
        response = Client().post('/lab-9/cookie/auth_login/',{'username':'utest','password':'ptest'}, follow=True)
        html_response = response.content.decode('utf8')
        self.assertIn("Reset Cookies (Logout)",html_response)

    def test_lab_9_clear_cookie_is_working(self):
        response = Client().get('/lab-9/cookie/clear/')
        self.assertEqual(response.status_code, 302)

    def test_api_enterkomputer(self):
    	self.assertEqual(type(get_drones().json()), list)
    	self.assertEqual(type(get_opticals().json()), list)
    	self.assertEqual(type(get_soundcards().json()), list)

#test custom auth
    def test_profile(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")
        response_post = self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
        response = self.client.get('/lab-9/profile/',follow=True)
        html_response = response.content.decode('utf8')
        self.assertIn('hafiyyan.sayyid',html_response)
        response = self.client.get(reverse('lab-9:index'), follow=True)
        self.assertNotIn('Login', response.content.decode('utf8'))
        self.assertEqual(self.client.session.get('drones', None), None)    
        self.assertEqual(self.client.session.get('drones', None), None)

        #add drone to session_drone
        self.client.get(reverse('lab-9:add_session_drones', kwargs={'id': 1}))
        self.client.get(reverse('lab-9:add_session_drones', kwargs={'id': 2}))
        self.assertEqual(len(self.client.session['drones']), 2)
        self.client.get(reverse('lab-9:del_session_drones', kwargs={'id': 1}))
        self.assertEqual(len(self.client.session['drones']), 1)
        self.client.get(reverse('lab-9:clear_session_drones'))
        self.assertEqual(len(self.client.session['data']), 5)

                #add drone to session_opticals
        self.client.get(reverse('lab-9:add_session_opticals', kwargs={'id': 1}))
        self.client.get(reverse('lab-9:add_session_opticals', kwargs={'id': 2}))
        self.assertEqual(len(self.client.session['opticals']), 2)
        self.client.get(reverse('lab-9:del_session_opticals', kwargs={'id': 1}))
        self.assertEqual(len(self.client.session['opticals']), 1)
        self.client.get(reverse('lab-9:clear_session_opticals'))
        self.assertEqual(len(self.client.session['data']), 5)

        #add drone to session_soundcards
        self.client.get(reverse('lab-9:add_session_soundcards', kwargs={'id': 1}))
        self.client.get(reverse('lab-9:add_session_soundcards', kwargs={'id': 2}))
        self.assertEqual(len(self.client.session['soundcards']), 2)
        self.client.get(reverse('lab-9:del_session_soundcards', kwargs={'id': 1}))
        self.assertEqual(len(self.client.session['soundcards']), 1)
        self.client.get(reverse('lab-9:clear_session_soundcards'))
        self.assertEqual(len(self.client.session['data']), 5)

        #test session item
        self.client.get(reverse('lab-9:add_session_item', kwargs={'key': 'drones', 'id': 1}))
        self.client.get(reverse('lab-9:add_session_item', kwargs={'key': 'drones', 'id': 2}))
        self.assertEqual(len(self.client.session['drones']), 2)
        self.client.get(reverse('lab-9:del_session_item', kwargs={'key': 'drones', 'id': 1}))
        self.assertEqual(len(self.client.session['drones']), 1)
        self.client.get(reverse('lab-9:clear_session_item', kwargs={'key': 'drones'}))
        self.assertEqual(len(self.client.session['data']), 5)


    def test_profile_false(self):
        self.username = 'a'
        self.password = 'a'
        response_post = self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
        response = self.client.get('/lab-9/profile/',follow=True)
        html_response = response.content.decode('utf8')
        self.assertIn('Username atau password salah',html_response)
        response = self.client.get(reverse('lab-9:index'))
        self.assertIn('Login', response.content.decode('utf-8'))
        response = self.client.get(reverse('lab-9:profile'), follow=True)
        self.assertIn('Login', response.content.decode('utf-8'))

    def test_auth_logout(self):
    	response_post = self.client.post('/lab-9/custom_auth/logout/')
    	response = self.client.get('/lab-9/',follow=True)
    	html_response = response.content.decode('utf8')
    	self.assertIn('Anda berhasil logout. Semua session Anda sudah dihapus',html_response)

#test csui_helper
    def test_get_access_token(self):
    	self.assertEqual(get_access_token('a','a'),None)

    def test_get_data_user(self):
    	access_token = get_access_token(env("SSO_USERNAME"), env("SSO_PASSWORD"))
    	self.assertEqual(type(get_data_user(access_token,'X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG')), dict)


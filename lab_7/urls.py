from django.conf.urls import url
from .views import index, add_friend, validate_npm, delete_friend, friend_list_json, friend_list, tambah_teman

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add-friend/$', add_friend, name='add-friend'),
    url(r'^validate-npm/$', validate_npm, name='validate-npm'),
    url(r'^tambah-teman/$', tambah_teman, name='tambah-teman'),
    url(r'^friend-list/$', friend_list, name='friend-list'),
    url(r'^friend-list/delete-friend/(?P<friend_id>\d+)/$', delete_friend, name='delete-friend'),
    url(r'^get-friend-list/$', friend_list_json, name='get-friend-list')
]

from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from django.http import Http404
from django.core.paginator import Paginator,EmptyPage, PageNotAnInteger
from .models import Friend, Mahasiswa
from .api_csui_helper.csui_helper import CSUIhelper
import os
import json

response = {}
csui_helper = CSUIhelper()

def index(request):
    mahasiswa_list = csui_helper.get_mahasiswa_list()
    for data in mahasiswa_list:
    	Mahasiswa.objects.get_or_create(nama=data['nama'],npm=data['npm'])
    friend_list = Friend.objects.all()
    mahasiswa_list = Mahasiswa.objects.all().order_by('pk')
    page = request.GET.get('page', 1)
    paginate_data = paginate_page(page, mahasiswa_list)
    mahasiswa = paginate_data['data']
    page_range = paginate_data['page_range']
    response = {"mahasiswa_list": mahasiswa, "friend_list": friend_list , "page_range" : page_range}
    html = 'lab_7/lab_7.html'
    return render(request, html, response)

#sumber : https://simpleisbetterthancomplex.com/tutorial/2016/08/03/how-to-paginate-with-django.html
def paginate_page(page, data_list):
    paginator = Paginator(data_list, 10)

    data = paginator.page(page)

    # Get the index of the current page
    index = data.number - 1
    # This value is maximum index of your pages, so the last page - 1
    max_index = len(paginator.page_range)
    # You want a range of 10, so lets calculate where to slice the list
    start_index = index if index >= 10 else 0
    end_index = 5 if index < max_index - 5 else max_index
    # Get our new page range. In the latest versions of Django page_range returns 
    # an iterator. Thus pass it to list, to make our slice possible again.
    page_range = list(paginator.page_range)[start_index:end_index]
    paginate_data = {'data':data, 'page_range':page_range}
    return paginate_data


def friend_list(request): #menampilkan data teman
    friend_list = Friend.objects.all()
    response['friend_list'] = friend_list
    html = 'lab_7/daftar_teman.html'
    return render(request, html, response)

# mengembalikan data berupa json
def friend_list_json(request): # update
    if(request.method == 'GET'):
        friends = [obj.as_dict() for obj in Friend.objects.all()]
        return JsonResponse({'status_code':200, "results": friends}, content_type='application/json')


@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        npm = request.POST['npm']
        name = request.POST['name']
        friend = Friend(friend_name=name, npm=npm)
        friend.save()
        return JsonResponse(friend.as_dict())

def delete_friend(request, friend_id):
    Friend.objects.filter(pk=friend_id).delete()
    return HttpResponseRedirect('/lab-7/')

@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)
    data = {
        'is_taken': check_friend(npm)
    }
    return JsonResponse(data)

@csrf_exempt
def tambah_teman(request):
	if(request.method == "POST"):
		npm = request.POST['npm']
		mahasiswaku = Mahasiswa.objects.get(npm=npm)
		friend = Friend(friend_name=mahasiswaku.nama, npm=npm)
		friend.save()
		return HttpResponseRedirect('/lab-7/')


def check_friend(haha):
    return Friend.objects.filter(npm=haha).count() > 0 

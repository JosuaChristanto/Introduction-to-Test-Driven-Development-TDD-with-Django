1.untuk menampilkan daftar mahasiswa fasilkom di django model
	a. sudah dilaksanakan oleh template saat method pada view.py
		memanggil csui_helper.get_mahasiswa_list()
	b. lalu setiap mahasiswa_list akan reslt hasil dari get_mahasiswa_list()
	c. memasukan semua mahasiswa kedalam list pada response dan di tampilkan
	di index
	d. menampilkan teman dengan memanggil method friend_list = Friend.objects.all() mengambil semua data pada friend dan ditampilkan di lab_8.html

2. pada tugas kedua juga sudah ada pada template, dimana saat button di klik
maka akan memanggil $("#add-friend").on("submit", function ()
dimana pada method ini kana mengambil nilai dari field_name & field NPM
dan kembali dimasukan dalam model

3. validate NPM digunakan dengan memanggil method $("#field_npm").change(function ()
	pada method ini akan memanggil url yg memanggil validasi NPM pada view.py
	jika data ditemukan pada model menurut NPM, maka dinyatakan success
	dan akan alert(...)

4. pagination sebenernya rada bingung, tetapi saya mengambil menurut referensi
#sumber : https://simpleisbetterthancomplex.com/tutorial/2016/08/03/how-to-paginate-with-django.html

bagian 2 :
1. membuat daftar teman dapat dilakukan dengan cara berikut
	a. memanggil methode get-friend-list melalui url
		tugas dari method diatas adalah mengubah semua data pada model menjadi dict
		dan dikembalikan dalam bentuk json response
	b. jika sukses, ambil data pada json repsonse
	c. buat html nya :)
2. menghapus teman dapat dilakukan dengan mencari menggunakan 'pk' pada model
pk dan id merupakan default yang di inisiasikan saat membuat suatu model

perintah dilaksanakan dengan window_open window.open('delete-friend/'+friend_id+'/', '_self');
dan melaksanakan method delet friend pada view.py

3. coverage 100%
	test pada model :
	membuat suatu model, dan lakukan print out pada model. dan bandingkan keluaran string model

	test pada view :
	untuk mengetes method pada view, kebanyakan adalah dengna membandingkan tipe
	return pada method tersebut. apakah JsonResponse/ HttpResponse, dkk

	tes pada csui_helper:
	dilakukan edit pada csui_helper dan meniadakan exception, karena test akan dicoba
	dengan memasukan id pass yg salah
	lalu hitung berapa jumlah mahasiswa yg diambil

	get_client_id : membandikan dengan string 'X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG'

	get authparam : mengambil dict.client_id dan membandingkan dengan 'X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG' 



PR :
1. add frend by nama saja / npm saja - DONE
	- masukan csui_helper.get_mahasiswa() kedalam model Mahasiswa
	- panggil method tambah_teman melalui URL
	- cari Mahasiswa.get(npm)
	- Buat object teman

2. cara paginator - DONE
	- paginator = Paginator(data_list, 10) >> setiap page akan menampilkan 10 buah data, dan page akhir sebanyak data%10
	- end_index = 5 if index < max_index - 5 else max_index >> menentukan banyaknya bar page yg ditampilkan

3. Memunculkan pipeline pada tampilan gitlab - DONE